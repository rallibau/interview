import org.junit.jupiter.api.Test;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapesTest {
    @Test
    public void out() throws Exception {
        String text = tapSystemOut(() -> {
            System.out.println("hello");
        });

        assertEquals("hello", text.trim());
    }

    @Test
    public void testTriangleDraw() throws Exception {
        String text = tapSystemOut(() -> {
           Triangle triangle = new Triangle();
           triangle.draw();
        });

        assertEquals("Soy un triangulo", text.trim());
    }

    @Test
    public void testTriangleCustomDraw() throws Exception {
        String text = tapSystemOut(() -> {
            Triangle triangle = new Triangle();
            triangle.customDraw("println \"Custom method\"");
        });

        assertEquals("Custom method", text.trim());
    }
}

