import org.junit.jupiter.api.*;
import questions.FlyingCar;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.*;

public class FlyingCarTest {

    @Test
    public void out() throws Exception {
        String text = tapSystemOut(() -> {
            System.out.println("hello");
        });

        assertEquals("hello", text.trim());
    }

    @Test
    public void flyinCarDrive() throws Exception {

        String text = tapSystemOut(() -> {
            FlyingCar flyingCar = new FlyingCar();
            flyingCar.drive();
        });
        assertEquals("I'm driving like a car", text.trim());
    }

    @Test
    public void flyinCarFly() throws Exception {
        String text = tapSystemOut(() -> {
            FlyingCar flyingCar = new FlyingCar();
            flyingCar.fly();
        });
        assertEquals("I'm flying like and aeroplane", text.trim());
    }


}
