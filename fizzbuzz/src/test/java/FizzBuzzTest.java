import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FizzBuzzTest {

    @ParameterizedTest(name = "{0} = {1}")
    @CsvSource({"0, FizzBuzz",
            "1 , 1",
            "3, Fizz",
            "4, 4",
            "5, Buzz",
            "15, FizzBuzz",
            "30, FizzBuzz",
            "100, Buzz"})
    void printMultiplesOfNumber(int number, String expectedResult) {
        assertEquals(expectedResult, NumberPrinter.printNumber(number));
    }

    @Test
    @DisplayName("prints the numbers from 1 to 100")
    void testOneToHundred() {
        for (int i = 1; i < 100; i++) {
            if (i % 3 == 0 && i % 5 == 0){
                assertEquals("FizzBuzz", NumberPrinter.printNumber(i));
            }else if (i % 3 == 0){
                assertEquals("Fizz", NumberPrinter.printNumber(i));
            }else if( i % 5 == 0){
                assertEquals("Buzz", NumberPrinter.printNumber(i));
            }else{
                assertEquals(String.valueOf(i), NumberPrinter.printNumber(i));
            }
        }
    }
}