public class NumberPrinter {

    public static String printNumber(int number) {
        String resultado = "";

        if (esMultiplo(number, 3)) {
            resultado = "Fizz";
        }
        if (esMultiplo(number, 5)) {
            resultado = resultado.concat("Buzz");
        }
        if (resultado.isEmpty()) {
            resultado = String.valueOf(number);
        }
        return resultado;
    }

    private static boolean esMultiplo(int n1, int n2) {
        if (n1 % n2 == 0)
            return true;
        else
            return false;
    }
}
