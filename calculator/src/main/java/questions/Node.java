package questions;


import groovy.lang.Binding;
import groovy.lang.GroovyShell;

public abstract class  Node {
    String operator;
    int value;
    Node leftNode;
    Node rightNode;

    public int calculate() {
        GroovyShell shell = new GroovyShell();
        return (Integer) shell.evaluate(writeScript());
    }

    protected String writeScript() {
        String script = "";
        if (operator != null) {
            script = " ".concat(operator).concat(" ");
        } else {
            script = " ".concat(String.valueOf(value)).concat(" ");
        }
        if(leftNode != null){
            script = leftNode.writeScript().concat(script);
        }
        if(rightNode != null){
            script = script.concat(rightNode.writeScript());
        }
        return script;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Node getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(Node leftNode) {
        this.leftNode = leftNode;
    }

    public Node getRightNode() {
        return rightNode;
    }

    public void setRightNode(Node rightNode) {
        this.rightNode = rightNode;
    }


    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
