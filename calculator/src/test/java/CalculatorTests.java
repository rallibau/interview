import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import questions.Node;
import questions.Operator;
import questions.Value;

import static org.junit.jupiter.api.Assertions.*;


public class CalculatorTests {
    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    void add(int first, int second, int expectedResult) {
        Node simpleAdd = new Operator("+");
        Node firstNode = new Value(first);
        Node secondNode = new Value(second);
        simpleAdd.setLeftNode(firstNode);
        simpleAdd.setRightNode(secondNode);
        assertEquals(expectedResult, simpleAdd.calculate(),
                () -> first + " + " + second + " should equal " + expectedResult);
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   0",
            "1,    2,   2",
            "49,  51, 2499",
            "1,  100, 100",
            "5,  5, 25"
    })
    void product(int first, int second, int expectedResult) {
        Node simpleAdd = new Operator("*");
        Node firstNode = new Value(first);
        Node secondNode = new Value(second);
        simpleAdd.setLeftNode(firstNode);
        simpleAdd.setRightNode(secondNode);
        assertEquals(expectedResult, simpleAdd.calculate(),
                () -> first + " + " + second + " should equal " + expectedResult);
    }

    @Test
    @DisplayName("2 + 3 * 4")
    void addsTwoNumbersAndMultiplication() {
        // 2 + 3 * 4
        Node times = new Operator("*");
        Node three = new Value(3);
        Node four = new Value(4);

        // first 3 * 4
        times.setLeftNode(three);
        times.setRightNode(four);

        // then 2 + result above
        Node two = new Value(2);
        Node plus = new Operator("+");

        plus.setLeftNode(two);
        plus.setRightNode(times);
        assertEquals(14, plus.calculate(), "2 + 3 * 4 should equal 14");
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   -1",
            "1,    2,   -1",
            "100,  51, 49",
            "15,  3, 12"
    })
    void substract(int first, int second, int expectedResult) {
        Node simpleSubstract = new Operator("-");
        Node firstNode = new Value(first);
        Node secondNode = new Value(second);
        simpleSubstract.setLeftNode(firstNode);
        simpleSubstract.setRightNode(secondNode);
        assertEquals(expectedResult, simpleSubstract.calculate(),
                () -> first + " + " + second + " should equal " + expectedResult);
    }

    @Test
    @DisplayName("2 + 3 * 4 -5")
    void addsTwoNumbersAndMultiplicationAndSubstract() {
        // 2 + 3 * 4
        Node times = new Operator("*");
        Node three = new Value(3);
        Node four = new Value(4);

        // first 3 * 4
        times.setLeftNode(three);
        times.setRightNode(four);

        // then 2 + result above
        Node two = new Value(2);
        Node plus = new Operator("+");

        plus.setLeftNode(two);
        plus.setRightNode(times);

        Node substract = new Operator("-");
        Node five = new Value(5);

        substract.setLeftNode(plus);
        substract.setRightNode(five);

        assertEquals(9, substract.calculate(), "2 + 3 * 4 -5 should equal 9");
    }

}
